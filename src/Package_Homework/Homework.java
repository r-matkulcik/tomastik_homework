package Package_Homework;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Tomastik.com
 */

public class Homework {

    public static void readFileAndPrintCounts(String File) throws FileNotFoundException {

        BufferedReader Buffer = null;
        int TotalWords = 0;
        int TotalLines = 0;
        int TotalCharacters = 0;
        int totalUppercaseWords = 0;
        int totalWordsStartedWithUppercase = 0;

        String Line;

        // Read file contents
        Buffer = new BufferedReader(new FileReader(File));

        try {
            Log("========== File Content ==========");

            // read each line one by one
            while ((Line = Buffer.readLine()) != null) {
                Log(Line);
                TotalLines++;
                //Find all words with uppercase letter
                for (int j = 0; j < Line.length(); j++) {
                    if (Line.charAt(j) >= 65 && Line.charAt(j) <= 90) {
                        totalUppercaseWords++;
                    }
                }


                // ignore multiple white spaces
                String[] myWords = Line.replaceAll("\\s+", " ").split(" ");
                for (String s : myWords) {

                    //Find all Characters
                    TotalCharacters += s.length();
                    
                }
                
                for (String s : myWords){
                    int g = 0;
                    g++;
                //Find all words started with uppercase letter
                    if (s.charAt(0) >= 65 && s.charAt(0) <= 90 ){
                        totalWordsStartedWithUppercase++;
                    }
                }
                

                TotalWords += myWords.length;


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log("\n========== Result ==========");

        Log("* Total Characters: " + TotalCharacters);
        Log("* Total Words: " + TotalWords);
        Log("* Total Lines: " + TotalLines);
        Log("* Total uppercase words: " + totalUppercaseWords);
        Log("* Total words who started with uppercase letter: " + totalWordsStartedWithUppercase);
    }
    //Print Function
    private static void Log(String string) {
        System.out.println(string);
    }

    public static void main(String[] args) {
        try {
            String filePath = System.getProperty("user.dir") + ("/src/Package_Homework/text.txt");
            readFileAndPrintCounts(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}